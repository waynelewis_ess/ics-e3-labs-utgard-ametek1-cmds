require autosave, 5.9.0
require ametek, master
require asyn, 4.33.0
require stream, 2.7.11
require calc

epicsEnvSet("SYS", "LabS-Utgard-VIP:")
epicsEnvSet("DEV", "SES-AmpLI-1")

epicsEnvSet("E3_CMDS_TOP", "/epics/iocs/e3")
epicsEnvSet("AMETEK_CMD", "$(E3_CMDS_TOP)/e3-ametek")
epicsEnvSet("AUTOSAVE_CMD", "$(E3_CMDS_TOP)/e3-autosave")
epicsEnvSet("AUTOSAVE_TOP", "$(E3_CMDS_TOP)/..")

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(ametek_DB)")

iocshLoad "$(AMETEK_CMD)/cmds/st.ametek7270.cmd" "SYS=$(SYS), DEV=$(DEV), AMETEK_IP=10.4.3.150"

iocshLoad "$(AUTOSAVE_CMD)/cmds/save_restore_before_init.cmd" "P=$(SYS),R=$(DEV),IOC=$(SYS)$(DEV),AS_TOP=$(AUTOSAVE_TOP)"

iocInit()

iocshLoad "$(AUTOSAVE_CMD)/cmds/save_restore_after_init.cmd" "P=$(SYS),R=$(DEV),IOC=$(SYS)$(DEV),AS_TOP=$(AUTOSAVE_TOP)"

